class Grid:
	instruction_columns = [[4], [3, 2], [1, 2, 1], [2, 1], [4, 2], [4]]  # pear
	instruction_rows = [[2], [1], [1, 1], [2, 1], [1, 1], [2, 2], [1, 1], [1, 1], [2, 2], [4]]

	# instruction_columns = [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]  # canvas
	# instruction_rows = [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]

	# instruction_columns = [[0],[3],[3],[3],[0]]
	# instruction_rows = [[0],[3],[3],[3],[0]]

	c_max = max(len(x) for x in instruction_columns)
	r_max = max(len(x) for x in instruction_rows)
	tiles = []
	tiles_by_rows = []
	tiles_by_columns = []

	@staticmethod
	def print_grid():
		tiles = Grid.tiles
		for i in range(len(tiles)):
			if i % len(Grid.instruction_columns) == 0 and i != 0:
				print('')
			if tiles[i].can_be_full:
				print('█', end='')
			elif tiles[i].can_be_crossed:
				print('░', end='')
			else:
				print('X', end='')
		print("\n")
