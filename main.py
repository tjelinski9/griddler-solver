import time
import pygame
from Grid import Grid
from Tile import Tile

pygame.init()
pygame.display.set_caption("Griddler Solver")
tile_size = 40


def get_coordinates(tile: Tile):
	no_of_row = Grid.tiles.index(tile) // len(Grid.instruction_columns)
	n = Grid.tiles.index(tile) // len(Grid.instruction_columns)
	no_of_column = Grid.tiles.index(tile) - len(Grid.instruction_columns) * n
	return no_of_column, no_of_row


def get_row_and_col(pos):
	x, y = pos
	no_of_row = x // tile_size - (Grid.r_max + 1)
	no_of_column = y // tile_size - (Grid.c_max + 1)
	return no_of_row, no_of_column


def draw_backtracking_indicator(window, color):
	# draws a circle - a pointer to the last changed tile
	no_of_column, no_of_row = get_coordinates(Grid.tiles[-1])
	start = (tile_size + Grid.r_max * tile_size, tile_size + Grid.c_max * tile_size)
	pygame.draw.circle(window, color, (start[0] + tile_size * (no_of_column + .5), start[1] + tile_size * (no_of_row + .5)), 9)
	pygame.display.update()
	# time.sleep(.05)


def main():
	global size, rows, columns
	rows = len(Grid.instruction_rows)
	columns = len(Grid.instruction_columns)
	size = ((rows * tile_size + (Grid.c_max + 1) * tile_size), columns * tile_size + Grid.r_max * tile_size)

	redraw()
	new_tile = Tile()

	# Grid.print_grid()

	play = True
	can_draw = True
	while play:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				play = False

			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_SPACE:  # solves puzzle
					can_draw = False
					Grid.tiles = []
					solve(new_tile)

				if event.key == pygame.K_s:  # saves edit
					instructions_rows = []
					instructions_columns = []

					Grid.tiles_by_rows = []
					for tile in Grid.tiles:
						if Grid.tiles.index(tile) % (len(Grid.instruction_columns)) == 0:
							Grid.tiles_by_rows.append([])
						Grid.tiles_by_rows[-1].append(tile)

					for row in Grid.tiles_by_rows:
						instructions_rows.append(convert_row_to_instructions(row))

					Grid.tiles_by_columns = []
					for i in Grid.instruction_columns:
						Grid.tiles_by_columns.append([])

					for tile in Grid.tiles:
						n = Grid.tiles.index(tile) // len(Grid.instruction_columns)
						Grid.tiles_by_columns[Grid.tiles.index(tile) - len(Grid.instruction_columns) * n].append(tile)

					for row in Grid.tiles_by_columns:
						instructions_columns.append(convert_row_to_instructions(row))

					Grid.instruction_rows = instructions_rows
					Grid.instruction_columns = instructions_columns
					print(instructions_rows)
					print(instructions_columns)
					can_draw = True

			if event.type == pygame.MOUSEBUTTONDOWN and can_draw:  # editor
				if len(Grid.tiles) == 0:
					for i in range(len(Grid.instruction_rows)*len(Grid.instruction_columns)):
						tile = Tile()
						tile.can_be_full = False
						tile.can_be_crossed = False
						Grid.tiles.append(tile)
				pos = pygame.mouse.get_pos()
				x, y = pos
				#
				start = tile_size + Grid.r_max * tile_size, tile_size + Grid.c_max * tile_size
				if start[0] < x < start[0] + tile_size * columns and start[1] < y < start[1] + tile_size * rows:
					row, col = get_row_and_col(pos)
					# print(row, col)
					Grid.tiles[len(Grid.instruction_columns)*col + row].set_next()

		redraw()


def convert_row_to_numbers(row):
	# returns list of 1s and 0s that make up a row
	# row - list of Tile class instances
	numbers = [1 if tile.can_be_full else 0 for tile in row]
	return numbers


def convert_row_to_instructions(row):
	# row - list of Tile class instances
	numbers = convert_row_to_numbers(row)

	result = []
	last = 0
	for i in range(len(numbers)):
		if numbers[i] == 0:
			last = 0
		elif numbers[i] > 0 and last == 0:
			result.append(numbers[i])
			last = 1
		elif numbers[i] > 0 and last == 1:
			result[-1] += 1

	if not result:
		result.append(0)

	return result


def validate(tiles):
	no_of_column, no_of_row = get_coordinates(tiles[-1])

	if not tiles[-1].can_be_crossed:  # failsafe
		return False

	Grid.tiles_by_rows = []
	for tile in tiles:
		if tiles.index(tile) % (len(Grid.instruction_columns)) == 0:
			Grid.tiles_by_rows.append([])
		Grid.tiles_by_rows[-1].append(tile)

	converted_row = convert_row_to_instructions(Grid.tiles_by_rows[no_of_row])
	if len(converted_row) > len(Grid.instruction_rows[no_of_row]):
		return False
	for i in range(len(converted_row) - 1):
		if converted_row[i] != Grid.instruction_rows[no_of_row][i]:
			tiles[-1].set_next()
			return False
	if converted_row[-1] > Grid.instruction_rows[no_of_row][
		len(converted_row) - 1]:
		return False
	if len(Grid.tiles_by_rows[no_of_row]) == len(Grid.instruction_columns) and len(converted_row) != len(
			Grid.instruction_rows[
				no_of_row]):
		return False

	Grid.tiles_by_columns = []

	for i in range(len(Grid.instruction_columns)):
		Grid.tiles_by_columns.append([])
	for tile in tiles:
		n = tiles.index(tile) // len(Grid.instruction_columns)
		Grid.tiles_by_columns[tiles.index(tile) - len(Grid.instruction_columns) * n].append(tile)

	converted_column = convert_row_to_instructions(Grid.tiles_by_columns[no_of_column])
	if len(converted_column) > len(Grid.instruction_columns[no_of_column]):
		tiles[-1].set_next()
		return False
	for i in range(len(converted_column) - 1):
		if converted_column[i] != Grid.instruction_columns[no_of_column][i]:
			tiles[-1].set_next()
			return False
	if converted_column[-1] > Grid.instruction_columns[no_of_column][
		len(converted_column) - 1]:
		return False
	if len(Grid.tiles_by_columns[no_of_column]) == len(Grid.instruction_rows) and len(converted_column) != len(
			Grid.instruction_columns[
				no_of_column]):
		return False

	if no_of_row > 0:
		for row_number in range(0, no_of_row):
			converted_row = convert_row_to_instructions(Grid.tiles_by_rows[row_number])
			if len(converted_row) != len(Grid.instruction_rows[row_number]):
				return False
			for i in range(0, len(converted_row)):
				if converted_row[i] != Grid.instruction_rows[row_number][i]:
					return False

	if no_of_column > 0 and no_of_row == len(Grid.instruction_rows) - 1:
		for col_number in range(0, no_of_column):
			converted_column = convert_row_to_instructions(Grid.tiles_by_columns[col_number])
			if len(converted_column) != len(Grid.instruction_columns[col_number]):
				return False
			for i in range(0, len(converted_column)):
				if converted_column[i] != Grid.instruction_columns[col_number][i]:
					return False

	if len(tiles) == len(Grid.instruction_columns) * len(Grid.instruction_rows):

		converted_row = convert_row_to_instructions(Grid.tiles_by_rows[-1])
		if len(converted_row) != len(Grid.instruction_rows[-1]):
			return False
		for i in range(0, len(converted_row)):
			if converted_row[i] != Grid.instruction_rows[-1][i]:
				return False

		converted_column = convert_row_to_instructions(Grid.tiles_by_columns[-1])
		if len(converted_column) != len(Grid.instruction_columns[-1]):
			return False
		for i in range(0, len(converted_column)):
			if converted_column[i] != Grid.instruction_columns[-1][i]:
				return False

	return True


def solve(tile: Tile):
	# recursive, main backtracking function

	if not tile.can_be_crossed:
		return False

	no_of_rows = len(Grid.instruction_rows)
	no_of_columns = len(Grid.instruction_columns)
	tiles = Grid.tiles

	if len(tiles) == no_of_rows * no_of_columns:
		return True  # solved

	tiles.append(tile)
	new_tile = Tile()
	window = pygame.display.set_mode((columns * tile_size + (Grid.r_max + 2) * tile_size, rows * tile_size + (
			Grid.c_max + 2) * tile_size))

	if validate(tiles):
		redraw()
		# draw_backtracking_indicator(window, "green")

		if solve(new_tile):
			return True

	new_tile = Tile()
	tiles[-1].set_next()
	if validate(tiles):
		redraw()
		# draw_backtracking_indicator(window, "green")

		if solve(new_tile):
			return True

	redraw()
	# draw_backtracking_indicator(window, "red")

	del tiles[-1]
	return False


def grid(window, size, rows, columns):
	x = 0
	y = 0
	window.fill((235, 235, 255))
	number_font = pygame.font.SysFont(None, tile_size)
	start = (tile_size + Grid.r_max * tile_size + .25 * tile_size, tile_size + Grid.c_max * tile_size + .25 * tile_size)
	for i in range(columns + 1):
		x += tile_size
		pygame.draw.line(window, (0, 0, 0), (x + Grid.r_max * tile_size, tile_size), (x + Grid.r_max * tile_size, size[0]))

	for i in range(rows + 1):
		y += tile_size
		pygame.draw.line(window, (0, 0, 0), (tile_size, y + Grid.c_max * tile_size), (tile_size + size[1], y + Grid.c_max * tile_size))

	for i in range(columns):
		for j in range(len(Grid.instruction_columns[i])):
			number_image = number_font.render(str(Grid.instruction_columns[i][j]), True, "black")
			window.blit(number_image, (start[0] + i * tile_size, start[1] - (len(Grid.instruction_columns[i]) - j) * tile_size))

	for i in range(rows):
		for j in range(len(Grid.instruction_rows[i])):
			number_image = number_font.render(str(Grid.instruction_rows[i][j]), True, "black")
			# window.blit(number_image, (start[0] + i*tileSize, start[1] - (len(Grid.instruction_columns[i]) - j)*tileSize))
			window.blit(number_image, (start[0] - (len(Grid.instruction_rows[i]) - j) * tile_size, start[1] + i * tile_size))


def board(window, size, rows, columns):
	start = (tile_size + Grid.r_max * tile_size, tile_size + Grid.c_max * tile_size)

	for tile in Grid.tiles:
		no_of_column, no_of_row = get_coordinates(tile)

		if tile.can_be_full and tile.can_be_crossed:
			pygame.draw.rect(window, "black", ((start[0] + tile_size * no_of_column, start[1] + tile_size * no_of_row), (tile_size, tile_size)))
		elif not tile.can_be_full and tile.can_be_crossed:
			pygame.draw.line(window, "black", (start[0] + tile_size * no_of_column, start[1] + tile_size * no_of_row), (start[0] + tile_size * (no_of_column + 1), start[1] + tile_size * (no_of_row + 1)))
			pygame.draw.line(window, "black", (start[0] + tile_size * (no_of_column + 1), start[1] + tile_size * no_of_row), (start[0] + tile_size * no_of_column, start[1] + tile_size * (no_of_row + 1)))
		else:
			pygame.draw.rect(window, (235, 235, 255), ((start[0] + tile_size * no_of_column + 1, start[1] + tile_size * no_of_row + 1), (tile_size - 1, tile_size - 1)))


def redraw():
	window = pygame.display.set_mode((len(Grid.instruction_columns) * tile_size + (Grid.r_max + 2) * tile_size, len(Grid.instruction_rows) * tile_size + (Grid.c_max + 2) * tile_size))
	window.fill((235, 235, 255))
	grid(window, size, rows, columns)
	board(window, size, rows, columns)

	pygame.display.update()


if __name__ == '__main__':
	main()
