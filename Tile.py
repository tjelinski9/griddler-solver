class Tile:
	def __init__(self):
		self.can_be_full = True
		self.can_be_crossed = True

	def set_next(self):  # changes from full to crossed, from crossed to incorrect
		if self.can_be_full and self.can_be_crossed:
			self.can_be_full = False
		elif not self.can_be_full and self.can_be_crossed:
			self.can_be_crossed = False
		else:
			self.can_be_full = True
			self.can_be_crossed = True